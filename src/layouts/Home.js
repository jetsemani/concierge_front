import React, { Component } from 'react'
//import { connect } from 'react-redux'
import { Link } from 'react-router'
import { bindActionCreators } from 'redux'

/* Strings */
import languages from '../helpers/strings.json'
const strings = languages.layouts['Home']
const stringsCommon = languages.common
import "../styles/main.scss"
import "../styles/login.scss"

import Login from './Login'

//var FilterTable = require('react-filter-table');
class Home extends Component {

    render() {
        //console.log(this.props)
        // Data for the table to display; can be anything
        let data = [
            { name: "Steve", age: 27, job: "Sandwich Eater" },
            { name: "Gary", age: 35, job: "Falafeler" },
            { name: "Greg", age: 24, job: "Jelly Bean Juggler" },
            { name: "Jeb", age: 39, job: "Burrito Racer" },
            { name: "Jeff", age: 48, job: "Hot Dog Wrangler" }
        ];

        // Fields to show in the table, and what object properties in the data they bind to
        let fields = [
            { name: 'name', displayName: "Name", inputFilterable: true, sortable: true },
            { name: 'age', displayName: "Age", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'job', displayName: "Occupation", inputFilterable: true, exactFilterable: true, sortable: true }
        ];
        return (
            <div className="container">
                <Login />
                {/* <FilterableTable
                    namespace="Records"
                    initialSort="name"
                    data={data}
                    fields={fields}
                    noRecordsMessage="There are no record to display"
                    noFilteredRecordsMessage="No record match your filters!"
                /> */}
            </div>
        )
    }

    saveEmployee = e => {

        this.setState({ name: "hgjh" })
        console.log("Se esta enviando algo")
    }
}

export default (Home)
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { bindActionCreators } from 'redux'
import axios from 'axios'
import serialize from 'form-serialize'
import FilterableTable from 'react-filterable-table'

import { showUserBar } from '../actions/user'
import { serverConfig } from '../localConfig'
import { InputNumber } from '../components/Inputs'
import FormAdd from '../components/FormAdd'
/* Strings */
import languages from '../helpers/strings.json'
const strings = languages.layouts['Login']
const stringsCommon = languages.common
import "../styles/main.scss"
import "../styles/login.scss"
import "../styles/pikaday.scss"
var DatePicker = require("react-bootstrap-date-picker");

import facturaImg from '../images/Factura.jpg'
var logoCompanyName = 'logoDefault.png' //Nesecitamos que se pueda leer y escribir, por eso uso Var
var logoCompany = require(`../images/${logoCompanyName}`)

import FilterTable from '../components/FilterTable'
const headers = [
    { id: 'status', type: 'text', label: 'Status', width: 100 },
    { id: 'type', type: 'text', label: 'Type', width: 200 },
    { id: 'title', type: 'text', label: 'Title', width: 300 },
    { id: 'category', type: 'text', label: 'Category', width: 200 },
    { id: 'subcategory', type: 'text', label: 'Subcategory', width: 200 },
    { id: 'date_created', type: 'date', label: 'Date created', width: 150 }]

const Modal = React.createClass({
    onClose() {
        var select = document.querySelector('html');
        select.style.overflow = 'auto';
    },
    render() {
        return (
            <div className="modal">
                {/*<Link className={cx("modalOuter")} to={this.props.returnTo}><i className={cx("material-icons", "modalClose")}>close</i></Link>*/}
                <Link className="modalOuter"></Link>
                <div className="modalContent">
                    <Link to={this.props.returnTo} onClick={this.onClose}>
                        <i style={{ color: 'gray' }} className="modalClose">close</i>
                    </Link>
                    {this.props.children}
                </div>
            </div>
        )
    }
})

if ('undefined' !== typeof window) {
    var Pikaday = require('pikaday')
}

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            autenticacion: null,
            records: null,
            add: false,
            signUp: false,
            commissionValue: '0',//Esta variable es para validacion, lo idea es terminar el componente de input
            showModal: false
        }
    }

    componentWillMount() {
        if (typeof (Storage) !== "undefined") {
            let access = localStorage.getItem("access")
            if (localStorage.getItem("logoCompanyName")) {
                let logoName = localStorage.getItem("logoCompanyName")
                try {
                    logoCompany = require(`../images/${logoName}`)
                } catch (error) {
                    console.log(`No se encontró la imagen ${logoName}, verificar el nombre de la imagen en la base de dato`)
                }
            }
            //console.log(localStorage.getItem("logoCompanyName"))
            //logoCompanyName = localStorage.getItem("logoCompanyName")
            //console.log(logoCompanyName)
            if (access) {
                this.setState({ authentication: "Cristobal Romero" })
                axios.defaults.headers.common['Authorization'] = access
                axios.get(`${serverConfig.apiUrl}/records`)
                    .then(records => {
                        if (typeof (Storage) !== "undefined") {
                            //this.setState({ records: records.data })
                            this.updateRecords()
                            localStorage.setItem("access", access)
                        } else {
                            //Cookies.remove('user');Removemos el usuario
                        }
                    })
            }
        } else {
            //Cookies.remove('user');Removemos el usuario
        }
    }

    componentDidUpdate(prevProps) {

        let isModal = (
            location.state &&
            location.state.modal &&
            this.previousChildren
        )
    }

    render() {
        let { authentication, records, add, signUp, showModal } = this.state
        let { location } = this.props

        if (location.query && location.query.signUp)
            console.log(location.query.signUp)

        // Data for the table to display; can be anything
        let data = records ? records : []
        // Fields to show in the table, and what object properties in the data they bind to
        let fields = [
            { name: 'vendor', displayName: "Vendedor", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'itemName', displayName: "Producto/Actividad", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'dateService', displayName: "Fecha de servicio", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'total', displayName: "Precio X PAX", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'itemCommission', displayName: "Comision", inputFilterable: true, exactFilterable: true, sortable: true },
            { name: 'supplierName', displayName: "Proveedor", inputFilterable: true, sortable: true },
            { name: 'supplierPhone', displayName: "Teléfono del proveedor", inputFilterable: true, sortable: true },
            { name: 'supplierEmail', displayName: "Correo del proveedor", inputFilterable: true, sortable: true },
            { name: 'clientName', displayName: "Cliente", inputFilterable: true, sortable: true },
            { name: 'clientPhone', displayName: "Teléfono del cliente", inputFilterable: true, sortable: true },
            { name: 'createdAt', displayName: "Fecha de creación", inputFilterable: true, sortable: true }
        ]

        let isModal = (
            location.state &&
            location.state.modal &&
            this.previousChildren
        )
        return (
            <div className="container" style={{ marginBottom: "200px" }}>
                {showModal &&
                    this.getModal()}
                {!authentication
                    ? signUp
                        ? this.getFormSignUp()
                        : this.getFormLogin()
                    : add ?
                        <FormAdd />
                        : <div>
                            <div className='center' style={{ maxHeight: '250px' }}>
                                <img style={{ maxHeight: '250px' }} src={logoCompany} />
                            </div>
                            <div className="row">
                                <p />
                                <button className="btn btn-primary" onClick={this.agregarRegistro}>Agregar nuevo registro</button>
                                <p />
                            </div>
                            <div className="row">
                                <h1>Registros</h1>
                            </div>
                            <div className="row">
                                <FilterableTable
                                    namespace="Records"
                                    initialSort="name"
                                    data={data}
                                    fields={fields}
                                    noRecordsMessage="There are no record to display"
                                    noFilteredRecordsMessage="No record match your filters!"
                                />
                            </div>
                            <div className="center">
                                <button className="btn btn-primary" style={{ margin: '10px' }} onClick={this.showModal}>Factura</button>
                                <a href="mailto:info@jetsemani.com?subject=Feedback%20mvp%20concierge" className="btn btn-primary" >Feedback</a>
                            </div>
                            {/* Se quiere cambiar la tabla para filtro, ya se inicio con algo */}
                            {/* <FilterTable headers={headers} rows={["", "", "", "", "", "", "", "", ""]} /> */}
                        </div>
                }
            </div>
        )
    }

    getModal = () => {
        return (<div className="modal">
            {/*<Link className={cx("modalOuter")} to={this.props.returnTo}><i className={cx("material-icons", "modalClose")}>close</i></Link>*/}
            <Link className="modalOuter"></Link>
            <div className="modalContent">
                <div>
                    <a onClick={this.showModal}  >
                        <i style={{ color: 'gray' }} className="modalClose material-icons">close</i>
                    </a>
                </div>
                <div>
                    <div>
                        <img src={facturaImg} />
                    </div>
                </div>
            </div>
        </div>)
    }

    showModal = (e) => {
        e.preventDefault()
        const { showModal } = this.state
        this.setState({ showModal: !showModal })
    }

    updateRecords = () => {
        axios.get(`${serverConfig.apiUrl}/records`)
            .then(records => {
                if (typeof (Storage) !== "undefined") {
                    let recordsData = []
                    for (let record of records.data) {
                        let obj = Object.assign({}, record)
                        obj = Object.assign(obj, obj.supplier)
                        let item = obj.items.shift()
                        recordsData.push(Object.assign(obj, item))
                    }

                    this.setState({ records: recordsData })
                } else {
                    //Cookies.remove('user');Removemos el usuario
                }
            })
    }

    getFormAdd = () => {
        const { commissionValue } = this.state
        return (
            <div className=" login-box2">
                <div className='center' style={{ maxHeight: '250px' }}>
                    <img style={{ maxHeight: '250px' }} src={logoCompany} />
                </div>
                <h1>Agregar registro</h1>
                <form onSubmit={this.handleOnAdd} id="formAdd">
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="col-sm-12">
                                <label>Vendedor:</label>
                                <input type="text" className="form-control" placeholder="Nombre del vendedor" name="vendor" />
                            </div>
                            <div className="col-sm-12">
                                <label>Nombre del producto o servicio:</label>
                                <input type="text" className="form-control" placeholder="Nombre del producto o servicio" name="itemName" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Fecha del servicio:</label>
                                <input type="text" className="form-control" placeholder="Fecha del servicio" name="dateService" id="dateService" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Total:</label>
                                <input type="number" className="form-control" placeholder="Total" name="itemTotal" required format='currency' />
                            </div>
                            <div className="col-sm-12">
                                <label>% de Comisión:</label>
                                <input type="number" className="form-control" placeholder="Comisión" name="itemCommission" required
                                    onChange={this.onchange.bind(this)}
                                    value={commissionValue}
                                />
                            </div>

                        </div>
                        <div className="col-sm-6">
                            <div className="col-sm-12">
                                <label>Proveedor:</label>
                                <input type="text" className="form-control" placeholder="Nombre del proveedor" name="supplierName" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Correo electrónico del proveedor:</label>
                                <input type="email" className="form-control" placeholder="example@mail.com" name="supplierEmail" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número telefónico del proveedor:</label>
                                <InputNumber type='number' placeholder='Teléfono' className="form-control" name="supplierPhone" />
                            </div>
                            <div className="col-sm-12">
                                <label>Cliente:</label>
                                <input type="text" className="form-control" placeholder="Nombre del cliente" name="clientName" />
                            </div>
                            <div className="col-sm-12">
                                <label>Correo electrónico del cliente:</label>
                                <input type="email" className="form-control" placeholder="example@mail.com" name="clientEmail" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número telefónico del cliente:</label>
                                <input type="number" className="form-control" placeholder="Teléfono" name="clientPhone" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número de PAX:</label>
                                <input type="text" className="form-control" placeholder="Número de PAX" name="pax" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número de la habitación:</label>
                                <input type="number" className="form-control" placeholder="Numero de la habitación" name="room" />
                            </div>
                        </div>
                    </div>

                    <div className="center" style={{ paddingTop: '20px' }}>
                        <button type="submit" className="btn btn-primary">{`Registrar`}</button>
                    </div>
                </form>
            </div>
        )
    }
    onchange = (e) => {
        const str = parseInt(e.target.value.toString())
        if (str > 100) {
            this.setState({ commissionValue: 100 })
            return
        }
        if (str < 0) {
            this.setState({ commissionValue: 0 })
            return
        }
        this.setState({ commissionValue: str })
    }
    switchLogin2signUp = (e) => {
        e.preventDefault()
        let { signUp } = this.state
        this.setState({ signUp: !signUp })
    }

    /**Campos para login y registro */
    getFormField = () => {
        return (
            <div>
                <div className="col-sm-12">
                    <label>Correo electrónico:</label>
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Correo electrónico" id="userName" required />
                        <span className="input-group-addon" id="basic-addon1">@</span>
                    </div>
                </div>
                <div className="col-sm-12">
                    <label>Contraseña:</label>
                    <div className="input-group">
                        <input type="password" className="form-control" placeholder="Contraseña" id="password" required />
                        <span className="input-group-addon" id="basic-addon1">@</span>
                    </div>
                </div>
            </div>
        )
    }

    getFormLogin = () => {
        return (
            <div className=" login-box">
                <h1>
                    Iniciar sesión
                </h1>
                <div className="row">
                    <form onSubmit={this.handleOnSubmit}>
                        {this.getFormField()}
                        <div className="center">
                            <button type="submit" className="btn btn-primary">Iniciar sesión</button>
                        </div>
                        <a className='center' onClick={this.switchLogin2signUp}>{'Registrarse'}</a>
                    </form>
                </div>
            </div>
        )
    }

    getFormSignUp = () => {
        return (
            <div className=" login-box">
                <h1>Registrarse</h1>
                <div className="row">
                    <form onSubmit={this.handleOnSignUp}>
                        {this.getFormField()}
                        <div className="center">
                            <button type="submit" className="btn btn-primary">{'Registrarse'}</button>
                        </div>
                        <a className='center' onClick={this.switchLogin2signUp}>{'Iniciar sesión'}</a>
                    </form>
                </div>
            </div>
        )
    }

    agregarRegistro = e => {
        this.setState({ add: true })
    }

    handleOnAdd = e => {
        e.preventDefault()
        var form = document.getElementById('formAdd');

        var str = serialize(form);
        var obj = serialize(form, { hash: true });
        console.log(obj)

        axios.post(`${serverConfig.apiUrl}/records`, obj)
            .then(access => {
                if (access.status == 201) {
                    this.updateRecords()
                }
            })

        this.setState({ add: false })
    }

    /**
     * manejador para el boton de inicio de sesión
     */
    handleOnSubmit = e => {
        e.preventDefault()
        let userName = document.getElementById("userName").value
        let password = document.getElementById("password").value
        this.login({
            email: userName,
            password
        })
    }

    handleOnSignUp = e => {
        e.preventDefault()
        let userName = document.getElementById("userName").value
        let password = document.getElementById("password").value

        axios.post
            (`${serverConfig.apiUrl}/users`, {
                "email": userName,
                "password": password
            })
            .then(access => {
                if (access.status == 201) {
                    this.setState({ authentication: "Cristobal Romero" })
                    axios.defaults.headers.common['Authorization'] = access.data.accessToken
                    /* axios.get(`${serverConfig.apiUrl}/records`)
                        .then(records => {
                            if (typeof (Storage) !== "undefined") {
                                this.setState({ records: records.data })
                                localStorage.setItem("access", access.data.accessToken)
                            } else {
                                //Cookies.remove('user');Removemos el usuario
                            }
                        }) */
                } else
                    console.log('Error de autenticacion')
            })
    }

    /**
     * Permite hacer una petición a la API para solicitar la autenticación
     * del usuario.
     */
    login = (data) => {
        data.strategy = "local"
        axios.post(`${serverConfig.apiUrl}/authentication`, data)
            .then(access => {
                if (access.status == 201) {
                    this.setState({ authentication: "Cristobal Romero" })
                    axios.defaults.headers.common['Authorization'] = access.data.accessToken

                    if (typeof (Storage) !== "undefined") {
                        this.updateRecords()

                        if (access.data.user && access.data.user.company) {
                            logoCompanyName = access.data.user.company.logo
                            logoCompany = require(`../images/${logoCompanyName}`)
                            localStorage.setItem("logoCompanyName", logoCompanyName)
                        }

                        localStorage.setItem("access", access.data.accessToken)
                    } else {
                        //Cookies.remove('user');Removemos el usuario
                    }
                } else
                    console.log('Error de autenticacion')
            })
    }
}

/**
 * Obtendremos el perfil del usuario
 */
/* getInforProfile = () => {

} */

const mapStateToProps = (state) => {
    return {
        location: state.location
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators({ showUserBar }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
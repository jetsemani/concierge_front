import React, { Component } from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'

//import { DropdownButton, MenuItem, Tabs, Tab, Alert, Modal } from 'react-bootstrap'

import Home from './Home'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'

/* Strings */
import languages from '../helpers/strings.json'
const strings = languages.layouts['Main']

class Main extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <NavBar />
                <Home />
                <Footer />
            </div>
        )
    }
}

export default (Main)
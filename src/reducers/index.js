import { combineReducers } from 'redux'

import locationReducer from '../store/location'
import { showUserBar } from '../actions/user'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    location: locationReducer,
    showUserBar,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer

import React from 'react'
import { Route, Router, browserHistory } from 'react-router'

import Main from './layouts/Main'

export default (
  <Router history={browserHistory}>
    <Route >
      <Route path='/' component={Main} />
    </Route>
  </Router>
)
import React, { Component } from 'react'

/**
 * Cuando necesitamos tener varias traducciones del lenguaje
 * las mezclamos en un solo objeto para centralizar las traducciones.
 */
import languages from '../helpers/strings.json'
const strFooter = languages.components['Footer']
const strCommon = languages['common']
const strings = Object.assign(strFooter, strCommon)

class Footer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <footer>
                <div className='center'>
                    <a id="logo-container" href="/" title="Home">
                        <label>Powered by</label>
                        <img src="http://jetsemani.io/app/themes/jetsemani-io-theme/assets/images/jetsemani-service-logo.png"
                            alt={strings.appTitle} title={strings.appTitle} style={{ height: 34 }} />
                    </a>
                </div>
            </footer>
        )
    }
}

export default (Footer)
import InputText from './InputText'
import InputNumber from './InputNumber'

export {
    InputText,
    InputNumber
}
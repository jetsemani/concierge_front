import React, { PropTypes } from 'react'

const translations = {
    remaining: {
        es: 'Restantes',
        en: 'Remaining'
    }
}

class InputText extends React.Component {
    constructor(props) {
        super(props)

        this.staticType = {
            text: 'text',
            textarea: 'textarea',
            tel: 'tel'
        }
        this.maxLengthDef = 524288 //Maximo de caracteres permitidos para un input

        let maxLength = props.maxLength
        let remaining = (props && maxLength && maxLength <= this.maxLengthDef)
            ? maxLength
            : this.maxLengthDef

        this.state = {
            remaining
        }
    }

    render() {
        const { maxLength, showRemaining, type, name, required, rows,
            placeholder, className, id } = this.props
        const language = this.props['language'] ? this.props.language : 'en'
        const { remaining } = this.state

        return (
            <div>
                {(type && type != this.staticType.textarea) ?
                    <input
                        type={type}
                        className={className ? className : null}
                        id={id ? id : 'inputText'}
                        name={name ? name : null}
                        required={required}
                        maxLength={maxLength ? maxLength : this.maxLengthDef}
                        onInput={this.remainingCharacters}
                        onKeyPress={this.validationCharacters}
                        placeholder={placeholder ? placeholder : null}
                    />
                    : <textarea
                        className={className ? className : null}
                        id={this.staticType.textarea}
                        name={name ? name : ''}
                        required={required}
                        rows={rows ? rows : 2}
                        maxLength={maxLength ? maxLength : this.maxLengthDef}
                        onInput={this.remainingCharacters}
                        onKeyPress={this.validationCharacters}
                    />}
                {(showRemaining || (type && type == this.staticType.textarea)) &&
                    <label>{translations.remaining[language]}<span> {remaining}</span></label>}
            </div>
        )
    }

    remainingCharacters = (e) => {
        /*   const { remaining } = this.state
          const { maxLength, type } = this.props
          let inputType = (type && type == this.staticType.text)
              ? this.staticType.text
              : this.staticType.textarea
  
          let input = document.getElementById(inputType)
          let characters = input.value.length
          let remainingCharacters = maxLength - characters
  
          this.setState({ remaining: remainingCharacters }) */
    }

    validationCharacters = (e) => {
        this.charactersValidation(/([A-Za-z0-9\,\.\:\;\ \?\¿\!\¡])/i, e)
    }

    charactersValidation = (patt, e) => {
        const { id } = this.props
        //Quede por aqui, hubo un problema al intentar capturar el valor del input
        let input = document.getElementById(id)
        console.log(input)
        let charIn = e.key
        /*         if (!patt.test(charIn))
                    e.preventDefault() */

        this.applyFormat(input.value)
    }

    applyFormat = (str) => {
        this.applyCurrency(str)
    }

    applyCurrency = (str) => {
        let strArray = str.split('')
        let newStrArray = []
        let tenth = 0
        for (let number of strArray) {
            if (tenth === 2) {
                newStrArray.push('.')
                newStrArray.push(number)
                tenth = 0
            } else {
                newStrArray.push(number)
                tenth++
            }
        }
        return newStrArray.join("")
    }
}

export default (InputText)
import React, { PropTypes } from 'react'

const translations = {
    remaining: {
        es: 'Restantes',
        en: 'Remaining'
    }
}

class InputNumber extends React.Component {
    constructor(props) {
        super(props)

        this.staticType = {
            text: 'text',
            textarea: 'textarea',
            tel: 'tel'
        }
        this.maxLengthDef = 524288 //Maximo de caracteres permitidos para un input

        let maxLength = props.maxLength
        let remaining = (props && maxLength && maxLength <= this.maxLengthDef)
            ? maxLength
            : this.maxLengthDef

        this.state = {
            remaining,
            valueChanged: ''
        }
    }

    /* componentWillMount() {
    } */

    /**
     * 
     */
    changeHandler = (e) => {
        //const str = this.numberWithCommas(e.target.value.toString())
        const str = e.target.value.toString()
        this.setState({ valueChanged: str })
    }

    render() {
        const { maxLength, showRemaining, type, name, required, rows,
            placeholder, className } = this.props
        const language = this.props['language'] ? this.props.language : 'en'
        const { remaining, valueChanged } = this.state

        return (
            <input
                type='number'
                className={className ? className : null}
                name={name ? name : null}
                value={valueChanged}
                required={required}
                maxLength={maxLength ? maxLength : this.maxLengthDef}
                onChange={this.changeHandler.bind(this)}
                placeholder={placeholder ? placeholder : null}
            />
        )
    }

    numberWithCommas = (str, separatedBy = '.') => {
        //Limpiando los caracteres de separación
        str = str.replace(separatedBy, '')

        console.log(str)
        if (str) {
            //Hay que verificar esta expresión regular
            return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separatedBy);
        }
        return str
    }
}

export default (InputNumber)
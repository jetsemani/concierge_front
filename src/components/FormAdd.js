import React, { Component } from 'react'

/**
 * Cuando necesitamos tener varias traducciones del lenguaje
 * las mezclamos en un solo objeto para centralizar las traducciones.
 */
import languages from '../helpers/strings.json'
const strings = languages.components['FormAdd']
const strCommon = languages['common']
//const strings = Object.assign(strFormAdd, strCommon)
import "../styles/login.scss"
import axios from 'axios'
import serialize from 'form-serialize'
import { serverConfig } from '../localConfig'
import { InputNumber } from '../components/Inputs'

import facturaImg from '../images/Factura.jpg'
var logoCompanyName = 'logoDefault.png' //Nesecitamos que se pueda leer y escribir, por eso uso Var
var logoCompany = require(`../images/${logoCompanyName}`)
if ('undefined' !== typeof window) {
    var Pikaday = require('pikaday')
}

class FormAdd extends Component {
    constructor(props) {
        super(props)

        this.state = {
            commissionValue: ''
        }
    }
    componentDidMount() {

        if (typeof (Storage) !== "undefined") {
            let access = localStorage.getItem("access")
            if (localStorage.getItem("logoCompanyName")) {
                let logoName = localStorage.getItem("logoCompanyName")
                try {
                    logoCompany = require(`../images/${logoName}`)
                } catch (error) {
                    console.log(`No se encontró la imagen ${logoName}, verificar el nombre de la imagen en la base de dato`)
                }
            }
            //console.log(localStorage.getItem("logoCompanyName"))
            //logoCompanyName = localStorage.getItem("logoCompanyName")
            //console.log(logoCompanyName)
            if (access) {
                this.setState({ authentication: "Cristobal Romero" })
                axios.defaults.headers.common['Authorization'] = access
                axios.get(`${serverConfig.apiUrl}/records`)
                    .then(records => {
                        if (typeof (Storage) !== "undefined") {
                            //this.setState({ records: records.data })
                            this.updateRecords()
                            localStorage.setItem("access", access)
                        } else {
                            //Cookies.remove('user');Removemos el usuario
                        }
                    })
            }
        } else {
            //Cookies.remove('user');Removemos el usuario
        }
        var startDate = new Pikaday({
            field: document.getElementById('dateService'),
            format: 'MM/DD/YYYY',
            onSelect: function () {
                //that.updateStartDate(this.getMoment())
            }
        })
    }

    render() {
        const { commissionValue } = this.state

        return (
            <div className=" login-box2">
                <div className='center' style={{ maxHeight: '250px' }}>
                    <img style={{ maxHeight: '250px' }} src={logoCompany} />
                </div>
                <h1>Agregar registro</h1>
                <form onSubmit={this.handleOnAdd} id="formAdd">
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="col-sm-12">
                                <label>*Vendedor:</label>
                                <input type="text" className="form-control" placeholder="Nombre del vendedor" name="vendor" required />
                            </div>
                            <div className="col-sm-12">
                                <label>*Nombre del producto o servicio:</label>
                                <input type="text" className="form-control" placeholder="Nombre del producto o servicio" name="itemName" required />
                            </div>
                            <div className="col-sm-12">
                                <label>*Fecha del servicio:</label>
                                <input type="text" className="form-control" placeholder="Fecha del servicio" name="dateService" id="dateService" required />
                            </div>
                            <div className="col-sm-12">
                                <label>*Total:</label>
                                <input type="number" className="form-control" placeholder="Total" name="itemTotal" format='currency' required />
                            </div>
                            <div className="col-sm-12">
                                <label>* % de Comisión:</label>
                                <input type="number" className="form-control" placeholder="Comisión" name="itemCommission" required
                                    onChange={this.onchange.bind(this)}
                                    value={commissionValue}
                                />
                            </div>

                        </div>
                        <div className="col-sm-6">
                            <div className="col-sm-12">
                                <label>*Proveedor:</label>
                                <input type="text" className="form-control" placeholder="Nombre del proveedor" name="supplierName" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Correo electrónico del proveedor:</label>
                                <input type="email" className="form-control" placeholder="example@mail.com" name="supplierEmail" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número telefónico del proveedor:</label>
                                <InputNumber placeholder='Teléfono' className="form-control" name="supplierPhone" />
                            </div>
                            <div className="col-sm-12">
                                <label>*Cliente:</label>
                                <input type="text" className="form-control" placeholder="Nombre del cliente" name="clientName" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Correo electrónico del cliente:</label>
                                <input type="email" className="form-control" placeholder="example@mail.com" name="clientEmail" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" />
                            </div>
                            <div className="col-sm-12">
                                <label>Número telefónico del cliente:</label>
                                <input type="number" className="form-control" placeholder="Teléfono" name="clientPhone" />
                            </div>
                            <div className="col-sm-12">
                                <label>*Número de PAX:</label>
                                <input type="number" className="form-control" placeholder="Número de PAX" name="pax" required />
                            </div>
                            <div className="col-sm-12">
                                <label>Número de la habitación:</label>
                                <input type="text" className="form-control" placeholder="Numero de la habitación" name="room" />
                            </div>
                        </div>
                    </div>

                    <div className="center" style={{ paddingTop: '20px' }}>
                        <button type="submit" className="btn btn-primary">{`Registrar`}</button>
                    </div>
                </form>
            </div>
        )
    }


    onchange = (e) => {
        const str = parseInt(e.target.value.toString())
        if (str > 100) {
            this.setState({ commissionValue: 100 })
            return
        }
        if (str < 0) {
            this.setState({ commissionValue: 0 })
            return
        }
        this.setState({ commissionValue: str })
    }

    handleOnAdd = e => {
        e.preventDefault()
        var form = document.getElementById('formAdd');

        var str = serialize(form);
        var obj = serialize(form, { hash: true });
        console.log(obj)

        axios.post(`${serverConfig.apiUrl}/records`, obj)
            .then(access => {
                if (access.status == 201) {
                    this.updateRecords()
                }
            })

        this.setState({ add: false })
        location.reload(true);
    }

}

export default (FormAdd)
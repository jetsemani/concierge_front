import React from 'react'
import { connect } from 'react-redux'
import ReactDOM from 'react-dom'
import moment from 'moment'
import { Table, Column, Cell } from 'fixed-data-table'

const TextCell = ({ rowIndex, type, rows, col, onSelectRow, ...props }) => (
    <Cell {...props} onClick={onSelectRow.bind(null, rows[rowIndex])}>
        {type == 'date' ? moment(rows[rowIndex][col]).format('MM/DD/YYYY') : rows[rowIndex][col]}
    </Cell>
)

export default class FilterTable extends React.Component {
    constructor(props) {
        super(props)

        this.rows = props.rows
        this.state = {
            filteredDataList: this.rows,
            sortBy: 'id',
            sortDir: null
        }
    }

    render() {
        const { language } = this.props
        const { filteredDataList } = this.state

        return (
            <Table id="filterTable"
                height={60 + ((filteredDataList.length > 15 ? 15 : filteredDataList.length) * 40)}
                width={1150}
                rowsCount={filteredDataList.length}
                rowHeight={40}
                headerHeight={60}>

                <Column
                    header={<Cell>Col 1</Cell>}
                    cell={<Cell>Column 1 static content</Cell>}
                    width={2000}
                />
                <Column
                    header={<Cell>Col 2</Cell>}
                    cell={<Cell>Column 2 static content</Cell>}
                    width={2000}
                />
            </Table>
        )
    }

    getColums() {
        const { headers } = this.props
        const { filteredDataList } = this.state
        var columns = []

        for (var column of headers) {
            console.log(column)
            columns.push(<Column
                key={'column-' + column.id}
                header={this._renderHeader(column.label, column.id)}
                cell={<TextCell type={column.type} rows={filteredDataList} col={column.id} onSelectRow={this.props.onSelectRow} />}
                width={column.width} />)
        }

        return columns
    }

    _renderHeader(label, cellDataKey) {
        var sortDirArrow = ''

        if (this.state.sortDir !== null) {
            sortDirArrow = this.state.sortDir === 'DESC' ? ' ↓' : ' ↑'
        }

        return <div style={{ padding: '0px 10px' }}>
            <a onClick={this._sortRowsBy.bind(this, cellDataKey)} style={{ cursor: 'pointer' }}>{label + (this.state.sortBy === cellDataKey ? sortDirArrow : '')}</a>
            <div>
                <input style={{ width: 100 + '%', height: 25 }} onChange={this._onFilterChange.bind(this, cellDataKey)} />
            </div>
        </div>
    }

    _onFilterChange(cellDataKey, event) {
        if (!event.target.value) {
            this.setState({
                filteredDataList: this.rows,
            })
        }

        var filterBy = event.target.value.toString().toLowerCase()
        var size = this.rows.length
        var filteredList = []

        for (var index = 0; index < size; index++) {
            var v = this.rows[index][cellDataKey]
            if (v && v.toString().toLowerCase().indexOf(filterBy) !== -1) {
                filteredList.push(this.rows[index])
            }
        }

        this.setState({
            filteredDataList: filteredList
        })
    }

    _sortRowsBy(cellDataKey) {
        var sortDir = this.state.sortDir
        var sortBy = cellDataKey

        if (sortBy === this.state.sortBy) {
            sortDir = this.state.sortDir === 'ASC' ? 'DESC' : 'ASC'
        } else {
            sortDir = 'DESC'
        }

        var rows = this.state.filteredDataList.slice()
        rows.sort((a, b) => {
            var sortVal = 0

            if (a[sortBy] > b[sortBy]) {
                sortVal = 1
            }
            if (a[sortBy] < b[sortBy]) {
                sortVal = -1
            }

            if (sortDir === 'DESC') {
                sortVal = sortVal * -1
            }

            return sortVal
        })

        this.setState({ sortBy, sortDir, filteredDataList: rows })
    }
}
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'

class App extends Component {
  static propTypes = {
    store: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
  }

  shouldComponentUpdate() {
    return false
  }

  render() {
    const { store, routes } = this.props

    return (
      <Provider store={store}>
        <div style={{ height: '100%' }} children={routes} />
      </Provider>
    )
  }
}

export default App

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import serialize from 'form-serialize'
import axios from 'axios'

import { showUserBar } from '../actions/user'

/* Strings */
import language from '../helpers/strings.json'
const strings = language.common

class NavBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
            authentication: null
        }
    }

    render() {
        const { authentication } = this.state
        /*
        const { user } = this.props */
        console.log(this.props)
        return (
            <nav className="navbar navbar-default">
                <div className="navbar-header">
                    {/* <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button> */}
                    <a id="logo-container" className="navbar-brand" href="/" title="Home">
                        <label>Powered by</label>
                        <img src="http://jetsemani.io/app/themes/jetsemani-io-theme/assets/images/jetsemani-service-logo.png"
                            alt={strings.appTitle} title={strings.appTitle} style={{ height: 34 }} />
                    </a>
                </div>
                <div id="navbar" className="navbar-collapse">
                    <div className="navbar-form navbar-right">
                        <button type="button" className="btn btn-success" onClick={this.logOut}>
                            {"Cerrar sesión"}
                        </button>
                    </div>
                </div>
            </nav>
        )
    }

    logOut = e => {
        const { showUserBar } = this.props.userActions
        showUserBar(false)
        if (typeof (Storage) !== "undefined") {
            let access = localStorage.getItem("access")
            if (access) {
                this.setState({ authentication: null })
                axios.defaults.headers.common['Authorization'] = null

                this.setState({ records: null })
                localStorage.removeItem("access")
                localStorage.removeItem("logoCompanyName")
                localStorage.removeItem("access")
            }
        } else {
            //Cookies.remove('user');Removemos el usuario
        }

        window.parent.location.reload()
    }
}

function mapStateToProps(state) {
    return {
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userActions: bindActionCreators({ showUserBar }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)
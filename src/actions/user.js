import * as types from '../types';

export function showUserBar(isShow = false) {
    return dispatch => {
        dispatch({ type: types.USER_LOGGED_IN, loggedIn: isShow })
    }
}